/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Orest Vasylyshyn
 */
public class Main {

	static Logger LOG = Logger.getLogger(Main.class.getName());

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		
		System.out.print("Please input file or folder name: ");
		
		BufferedReader cliReader = new BufferedReader(new InputStreamReader(System.in));

		String inputtedPath = null;

		try {
			inputtedPath = cliReader.readLine();
		} catch (IOException ex) {
			LOG.log(Level.SEVERE, "Read line error", ex);
		}

		if (Objects.isNull(inputtedPath)) {
			System.out.println("Provided path not defined, sorry(");
			return;
		}
		
		PathProcessor pathProcessor = new PathProcessor(inputtedPath);

		pathProcessor.showPathData();

	}

}