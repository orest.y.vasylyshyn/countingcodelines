/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Orest Vasylyshyn
 */
public class PathData {
	
	private String name;
	
	private long sourceCodeLinesCount;
	
	private List<PathData> subPaths;
	
	public PathData() {
		subPaths = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSourceCodeLinesCount() {
		return sourceCodeLinesCount;
	}

	public void setSourceCodeLinesCount(long sourceCodeLinesCount) {
		this.sourceCodeLinesCount = sourceCodeLinesCount;
	}

	public List<PathData> getSubPaths() {
		return subPaths;
	}

	public void setSubPaths(List<PathData> subPaths) {
		this.subPaths = subPaths;
	}
	
	
	
}
