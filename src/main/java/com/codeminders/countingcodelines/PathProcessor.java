/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;

/**
 *
 * @author Orest Vasylyshyn
 */
public class PathProcessor {
	
	static final String LEVEL_PREFIX = "└─";
	static final String LEVEL_MARGIN = "  ";
	
	private final Logger LOG = Logger.getLogger(getClass().getName());

	private Path providedPath;

	PathProcessor(String path) {
		providedPath = Paths.get(path);
	}
	
	private long filesCount(Path path) {
		long filesCount = 0;
		
		try {
			filesCount = Files.walk(path)
							.filter(Files::isRegularFile)
							.count();
		} catch (IOException ex) {
			LOG.log(Level.SEVERE, "Receiving files count error", ex);
		}
		
		return filesCount;
	}
	
	private PathData getPathData(Path path) {
		PathData result = new PathData();
		
		result.setName(path.toFile().getName());
		
		AtomicLong totalCount = new AtomicLong(0);
		
		File file = path.toFile();
		
		if (file.isDirectory()) {
			Arrays.stream(file.listFiles())
							.sorted((o1, o2) -> {
								return ((o1.isDirectory() && o2.isDirectory()) || (o1.isFile() && o2.isFile())) ? o1.getName().compareTo(o2.getName()) : (o1.isDirectory() && o2.isFile()) ? -1 : 1;
							})						
							.forEach((item) -> {
					PathData pathData = getPathData(item.toPath());
					result.getSubPaths().add(pathData);
					totalCount.addAndGet(pathData.getSourceCodeLinesCount());
			});
		} else if (file.isFile()) {
			long count = FileProcessor.sourceCodeLinesCount(file);
			totalCount.addAndGet(count);
		}
		
		result.setSourceCodeLinesCount(totalCount.get());
		
		return result;
	}
	
	private void printPathData(final PathData pathData, final int level) {		
		String levelPrefix = "";
		
		if (level > 0) {
			levelPrefix += LEVEL_PREFIX;
		}
		
		for (int i = 2; i <= level; i++) {
			levelPrefix = LEVEL_MARGIN + levelPrefix;
		}
		
		System.out.println(String.format("%s%s: %d", levelPrefix, pathData.getName(), pathData.getSourceCodeLinesCount()));
		
		if (!pathData.getSubPaths().isEmpty()) {
			pathData.getSubPaths().forEach(item -> {
				printPathData(item, level + 1);
			});
		}
	}
	
	private void printPathData(PathData pathData) {
		printPathData(pathData, 0);
	}
	
	public PathData getPathData() {
		return getPathData(providedPath);
	}
	
	public void showPathData() {
		if (Objects.isNull(providedPath.getRoot())) {
			System.out.println("Provided path not contains root, sorry(");
			return;
		}
		
		if (filesCount(providedPath) <= 0) {
			System.out.println("Provided path not contains any files, sorry(");
			return;
		}
		
		PathData pathData = getPathData(providedPath);
		
		printPathData(pathData);
		
	}
}
