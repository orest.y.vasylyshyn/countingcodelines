/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Orest Vasylyshyn
 */
public class FileProcessor {	

	static final String LINE_COMMENT = "//";
	static final String BLOCK_COMMENT_BEGIN = "/*";
	static final String BLOCK_COMMENT_END = "*/";
	static final String QUOTE = "\"";

	static Logger LOG = Logger.getLogger(FileProcessor.class.getName());

	static long sourceCodeLinesCount(File file) {
		long result = 0;

		BufferedReader bufferedReader = null;
		String line = null;
		boolean inBlockComment = false;

		try {
			FileReader fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				
				line = line.trim();
				
				if (inBlockComment) {
					if (commentEnded(line)) {
						line = line.substring(line.indexOf(BLOCK_COMMENT_END) + 2).trim();
						inBlockComment = false;
						if (isEmptyLine(line) || isLineComment(line)) {
							continue;
						}
					} else {
						continue;
					}
				} else {
					if (isEmptyLine(line) || isLineComment(line)) {
						continue;
					}
				}
				
				if (isSourceCodeLine(line)) {
					result++;
				}
				
				if (commentBegan(line)) {
					inBlockComment = true;
				}
				
			}

		} catch (Exception ex) {
			LOG.log(Level.SEVERE, "Read line error", ex);
		}

		return result;
	}
	
	private static boolean isEmptyLine(String line) {
		return line.isEmpty();
	}
	
	private static boolean isLineComment(String line) {
		return line.startsWith(LINE_COMMENT);
	}

	private static boolean commentBegan(String line) {
		int index = line.indexOf(BLOCK_COMMENT_BEGIN);
		if (index < 0) {
			return false;
		}
		int quoteStartIndex = line.indexOf(QUOTE);
		if (quoteStartIndex != -1 && quoteStartIndex < index) {
			while (quoteStartIndex > -1) {
				line = line.substring(quoteStartIndex + 1);
				int quoteEndIndex = line.indexOf(QUOTE);
				line = line.substring(quoteEndIndex + 1);
				quoteStartIndex = line.indexOf(QUOTE);
			}
			return commentBegan(line);
		}
		return !commentEnded(line.substring(index + 2));
	}

	private static boolean commentEnded(String line) {
		int index = line.indexOf(BLOCK_COMMENT_END);
		if (index < 0) {
			return false;
		} else {
			String subString = line.substring(index + 2).trim();
			if (isEmptyLine(line) || isLineComment(line)) {
				return true;
			}
			if (commentBegan(subString)) {
				return false;
			} else {
				return true;
			}
		}
	}

	private static boolean isSourceCodeLine(String line) {
		boolean isSourceCodeLine = false;

		if (isEmptyLine(line) || isLineComment(line)) {
			return isSourceCodeLine;
		}
		
		if (line.length() == 1) {
			return true;
		}
		
		int index = line.indexOf(BLOCK_COMMENT_BEGIN);
		if (index != 0) {
			return true;
		} else {
			while (line.length() > 0) {
				line = line.substring(index + 2);
				int endCommentPosition = line.indexOf(BLOCK_COMMENT_END);
				if (endCommentPosition < 0) {
					return false;
				}
				if (endCommentPosition == line.length() - 2) {
					return false;
				} else {
					String subString = line.substring(endCommentPosition + 2).trim();
					if (isEmptyLine(subString) || isLineComment(subString)) {
						return false;
					} else {
						if (subString.startsWith(BLOCK_COMMENT_BEGIN)) {
							line = subString;
							continue;
						}
						return true;
					}
				}

			}
		}
		
		return isSourceCodeLine;
	}

}
