/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Orest Vasylyshyn
 */
public class PathProcessorTest {
	
	private static PathProcessor pathProcessor;
	
	private static final String TEST_PATH = "/testData";
	
	@BeforeClass
	public static void init() {
		pathProcessor = new PathProcessor(PathProcessorTest.class.getClass().getResource(TEST_PATH).getPath());
		pathProcessor.showPathData();
	}
	
	@Test
	public void testSourceCodeLinesInFolder() {
		PathData pathData = pathProcessor.getPathData();
		
		Assert.assertEquals("all source code lines count in folder", 42, pathData.getSourceCodeLinesCount());
		
		Assert.assertEquals("source code lines count in subFolder3 ", 17, pathData.getSubPaths().get(2).getSourceCodeLinesCount());
		
		Assert.assertEquals("source code lines count in subFolder2 testFile21", 6, pathData.getSubPaths().get(1).getSubPaths().get(0).getSourceCodeLinesCount());
	}
	
}