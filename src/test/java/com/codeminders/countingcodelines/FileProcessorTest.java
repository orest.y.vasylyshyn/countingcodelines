/*
 * Copyright 2020 Orest Vasylyshyn.
 */
package com.codeminders.countingcodelines;

import java.io.File;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Orest Vasylyshyn
 */
public class FileProcessorTest {
	
	private static File file;
	
	private static final String TEST_FILE = "/testData/testFile.code";
	
	@BeforeClass
	public static void init() {
		file = new File(FileProcessorTest.class.getClass().getResource(TEST_FILE).getPath());
	}
	
	@Test
	public void testSourceCodeLinesCount() {
		Assert.assertEquals("source code lines count", 5, FileProcessor.sourceCodeLinesCount(file));
	}
	
}
